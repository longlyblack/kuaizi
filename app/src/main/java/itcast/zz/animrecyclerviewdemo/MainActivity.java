package itcast.zz.animrecyclerviewdemo;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sch.rfview.AnimRFGridLayoutManager;
import com.sch.rfview.AnimRFRecyclerView;

import java.util.ArrayList;
import java.util.List;

import itcast.zz.animrecyclerviewdemo.databinding.ActivityMainBinding;

/**
 * 这个demo 只能下拉到底部的时候加载一次
 *
 */

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mainBinding;
    private View headerView;
    private View footerView;
    private Context context;
    private List<String> datas ;
    private Handler handler = new  Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        context = this;
        addData();
        initView();
    }

    private void initView() {
        headerView = LayoutInflater.from(MainActivity.this).inflate(R.layout.header, null);
        footerView = LayoutInflater.from(MainActivity.this).inflate(R.layout.footer, null);

        //配置recyclerView 的必要语句
        /**
         * 设置管理器，每行两个item
         */
        mainBinding.recycler.setLayoutManager(new AnimRFGridLayoutManager(MainActivity.this,3));
        /**
         * 添加头布局
         */
        mainBinding.recycler.addHeaderView(headerView);
        /**
         * 设置头布局的拉伸，默认1.5f,必须写在setHeaderIamge方法之前
         */
        mainBinding.recycler.setScaleRatio(2.0f);
        /**
         * 设置头布局的图片，不设置就用默认的
         */
        mainBinding.recycler.setHeaderImage((ImageView) headerView.findViewById(R.id.image_my));
        /**
         * 设置footer布局
         */
        mainBinding.recycler.addFootView(footerView);
        /**
         * 设置刷新动画的颜色
         */
        mainBinding.recycler.setColor(Color.BLUE,Color.GREEN);
        /**
         * 设置头部动画恢复的执行时间,  默认500毫秒
         */
        mainBinding.recycler.setHeaderImageDurationMillis(300);
        /**
         * 设置拉伸到最高时，头部的透明度  默认0.5f
         */
        mainBinding.recycler.setHeaderImageMinAlpha(0.6f);
        /**
         * 设置适配器
         */
        mainBinding.recycler.setAdapter(new MyAdapter());
        /**
         * 设置刷新和加载的监听
         */
        mainBinding.recycler.setLoadDataListener(new AnimRFRecyclerView.LoadDataListener() {
            /**
             * 刷新
             */
            @Override
            public void onRefresh() {
                new Thread(new MyRunnable(true)){}.start();
                Toast.makeText(context,"顶部,刷新",Toast.LENGTH_SHORT).show();
            }

            /**
             * 加载
             */
            @Override
            public void onLoadMore() {
                new Thread(new MyRunnable(false)){}.start();
                Toast.makeText(context,"底部，加载",Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 需要执行耗时操作开始加载数据的操作
     */
    class MyRunnable implements Runnable {
        boolean isRefresh;

        public  MyRunnable(boolean isRefrash){
            this.isRefresh = isRefrash;
        }
        @Override
        public void run() {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (isRefresh) {
                        newData();
                        mainBinding.recycler.getAdapter().notifyDataSetChanged();
                    }else{
                        addData();
                        mainBinding.recycler.getAdapter().notifyDataSetChanged();
                    }
                }
            });
        }
    }

    /**
     * 添加数据
     */
    private void addData() {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        for (int i = 0; i < 36; i++) {
            datas.add("刚开始条目  " + (datas.size() + 1));
        }
        Toast.makeText(context,"条目长度"+ datas.size(),Toast.LENGTH_SHORT).show();
    }
    public void newData() {
        datas.clear();
        for (int i = 0; i < 36; i++) {
            datas.add("刷新后条目  " + (datas.size() + 1));
        }
    }

    /***************************recyclerView的适配器*******************************************/
    private class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            TextView view = new TextView(MainActivity.this);
            view.setGravity(Gravity.CENTER);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    DimensionConvert.dip2px(MainActivity.this, 50)));
            MyViewHolder mViewHolder = new MyViewHolder(view);
            return mViewHolder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.mTextView.setText(datas.get(position));
        }

        @Override
        public int getItemCount() {
            return datas.size();
        }
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView;
        }
    }
}
